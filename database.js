const sqlite = require("sqlite")
const sqlite3 = require("sqlite3").verbose()
const generator = require("./generator")
const axios = require("axios")
const amqp = require("amqplib")

async function initDB() {
  try {
    const db = await sqlite.open({
      filename: "./airport.db",
      driver: sqlite3.Database,
    })

    await db.exec(`CREATE TABLE IF NOT EXISTS flights (
      id TEXT PRIMARY KEY,
      linkedId TEXT,
      type TEXT NOT NULL,
      status TEXT NOT NULL,
      point_a_city TEXT NOT NULL,
      point_a_time TEXT NOT NULL,
      point_b_city TEXT NOT NULL,
      point_b_time TEXT NOT NULL,
      airplane_capacity INTEGER NOT NULL
    )`)

    console.log('Таблица "flights" успешно создана или уже существует')

    await checkAndGenerateFlights(db)
    const rows = await db.all(
      "SELECT * FROM flights WHERE status NOT IN ('Прилетел', 'Улетел', 'Отменен')"
    )
    rows.forEach(async (el) => {
      await manageFlightStatuses(el, db)
    })
    return db
  } catch (error) {
    console.error("Ошибка при работе с базой данных", error)
  }
}

async function checkAndGenerateFlights(db) {
  try {
    const now = new Date()
    const rows = await db.all(
      "SELECT MAX(point_b_time) AS lastArrivalTime FROM flights WHERE type = 'arrive'"
    )
    const lastArrivalTime = rows[0]?.lastArrivalTime
      ? new Date(rows[0].lastArrivalTime)
      : null

    let generateFromTime
    let durationInMinutes

    if (!lastArrivalTime || lastArrivalTime < now) {
      // Кейс 1: Первый запуск или все рейсы уже прилетели
      generateFromTime = new Date(now.getTime() + 4 * 60000) // Текущее время + 4 минуты
      durationInMinutes = 24
    } else if (lastArrivalTime - now <= 4 * 60000) {
      // Кейс 5: Последний рейс прибудет менее чем через 4 минуты
      generateFromTime = new Date(lastArrivalTime.getDate() + 4 * 60000)
      durationInMinutes = 12
    } else if (lastArrivalTime - now <= 12 * 60000) {
      // Кейс 2 и 3: Последний рейс прибудет менее чем через 12 минут
      generateFromTime = lastArrivalTime
      durationInMinutes = 12
    } else {
      // Нет необходимости в генерации новых рейсов
      return
    }

    const flights = generator(generateFromTime.toISOString(), durationInMinutes)
    console.log(`Сгенерированно ${flights.length * 2} рейсов`)
    await insertFlights(flights, db)
  } catch (error) {
    console.error("Ошибка при проверке и генерации рейсов:", error)
  }
}

// Функция для вставки сгенерированных рейсов в БД
// Улучшенная функция insertFlights с правильной передачей db
async function insertFlights(flights, db) {
  for (let flightPair of flights) {
    for (let flight of flightPair) {
      await db.run(
        `INSERT INTO flights (id, linkedId, type, status, point_a_city, point_a_time, point_b_city, point_b_time, airplane_capacity) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)`,
        [
          flight.id,
          flight.linkedId,
          flight.type,
          flight.status,
          flight.point_a_city,
          flight.point_a_time,
          flight.point_b_city,
          flight.point_b_time,
          flight.airplane_capacity,
        ]
      )
      // Правильная передача db в getFlight
      const flightFromBd = await getFlight(flight.id, db)
      // Правильная передача db в manageFlightStatuses
      await manageFlightStatuses(flightFromBd, db)
    }
  }
}
async function getFlight(id, db) {
  try {
    const selectResult = await db.all("SELECT * FROM flights WHERE id = ?", [
      id,
    ])
    if (!selectResult[0]) {
      throw `Рейс ${id} не найден.`
    }
    return selectResult[0]
  } catch (error) {
    console.error(error)
  }
}
async function changeStatusFlight(status, id, db) {
  try {
    await db.run("UPDATE flights SET status = ? WHERE id = ?", [status, id])
    return await getFlight(id, db)
  } catch (error) {
    console.error(error)
  }
}

async function sendMessage(queueName, message, key) {
  try {
    // Параметры подключения
    const amqpServer =
      "amqps://itojxdln:DEL8js4Cg76jY_2lAt19CjfY2saZT0yW@hawk.rmq.cloudamqp.com/itojxdln"
    const connection = await amqp.connect(amqpServer)
    const channel = await connection.createChannel()
    await channel.assertQueue(queueName, {
      durable: false,
    })

    // Create bindings
    await channel.bindQueue(queueName, "PassengersExchange", key)

    // BasicPublish
    channel.publish("PassengersExchange", key, Buffer.from(message))
    console.log(`Message sent to queue ${queueName}: ${message}`)
    setTimeout(() => {
      connection.close()
    }, 5000) // Дайте некоторое время на отправку сообщения перед закрытием соединения
  } catch (error) {
    console.error(`Error sending message to queue ${queueName}`)
  }
}

async function manageFlightStatuses(flight, db) {
  const now = new Date()
  const pointATime = new Date(flight.point_a_time)
  const pointBTime = new Date(flight.point_b_time)

  // Для рейсов типа "arrive"
  if (flight.type === "arrive") {
    if (pointBTime - now > 60000) {
      // Более чем за минуту до прилета
      setTimeout(async () => {
        console.log(`Попытка генерации самолета для рейса ${flight.id}`)
        const flightFromBase = await getFlight(flight.id, db)
        if (flightFromBase.status === "В полете") {
          try {
            const selectResult = await db.all(
              "SELECT * FROM flights WHERE linkedId = ? AND type = 'departure'",
              [flightFromBase.linkedId]
            )
            if (!selectResult[0]) {
              throw `Рейс ${flightFromBase.linkedId} не найден.`
            }
            const flight_second_from_base = selectResult[0]
            const response = await axios.post(
              "http://46.174.48.185:9008/v1/airplanes/generate/voyage",
              [flightFromBase, flight_second_from_base],
              {
                headers: {
                  "Content-Type": "application/json",
                },
              }
            )
            if (response.status === 200) {
              console.log(`Смена статуса на Посадка ${flight.id}`)
              await changeStatusFlight("Посадка", flight.id, db)
            } else {
              throw "Ошбка"
            }
          } catch (error) {
            try {
              const selectResult = await db.all(
                "SELECT * FROM flights WHERE linkedId = ? AND type = 'departure'",
                [flightFromBase.linkedId]
              )
              if (!selectResult[0]) {
                console.log(`Рейс ${flightFromBase.linkedId} не найден.`)
              }
              const flight_second_from_base = selectResult[0]
              console.log(`Смена статуса на Отменен ${flight.id}`)
              await changeStatusFlight("Отменен", flight.id, db)
              console.log(
                `Смена статуса на Отменен ${flight_second_from_base.id}`
              )
              await changeStatusFlight(
                "Отменен",
                flight_second_from_base.id,
                db
              )
              sendMessage(
                "FlightsToRegistration",
                JSON.stringify({
                  Flight: flight_second_from_base.id,
                  Registration: 2,
                }),
                "FlightsToRegistrationKey"
              )
            } catch (error) {}
          }
        }
      }, pointBTime - now - 60000)
      setTimeout(async () => {
        const flight_from_base = await getFlight(flight.id, db)
        if (flight_from_base.status === "Посадка") {
          console.log(`Смена статуса на Задерживается ${flight.id}`)
          await changeStatusFlight("Задерживается", flight.id, db)
        }
      }, pointBTime - now)
    } else if (flight.status === "Посадка") {
      // Менеее часа до прилета со статусом посадка
      setTimeout(
        async () => {
          const flight_from_base = await getFlight(flight.id, db)
          if (flight_from_base.status === "Посадка") {
            console.log(`Смена статуса на Задерживается ${flight.id}`)
            await changeStatusFlight("Задерживается", flight.id, db)
          }
        },
        pointBTime - now > 0 ? pointBTime - now : 1
      )
    } else {
      // Продолбали рейс - отменяем
      const flight_from_base = await getFlight(flight.id, db)
      if (flight_from_base.status === "В полете") {
        console.log(`Смена статуса на Отменен ${flight.id}`)
        await changeStatusFlight("Отменен", flight.id, db)
        try {
          const selectResult = await db.all(
            "SELECT * FROM flights WHERE linkedId = ? AND type = 'departure'",
            [flight.linkedId]
          )
          if (!selectResult[0]) {
            throw `Рейс ${flight.linkedId} не найден.`
          }
          const flight_second_from_base = selectResult[0]
          console.log(`Смена статуса на Отменен ${flight_second_from_base.id}`)
          await changeStatusFlight("Отменен", flight_second_from_base.id, db)
          sendMessage(
            "FlightsToRegistration",
            JSON.stringify({
              Flight: flight_second_from_base.id,
              Registration: 2,
            }),
            "FlightsToRegistrationKey"
          )
        } catch (error) {
          console.error(error)
        }
      }
    }
  }

  // Для рейсов типа "departure"
  if (flight.type === "departure") {
    if (pointATime - now > 60000) {
      // Более чем за минуту до вылета
      setTimeout(
        async () => {
          // Здесь должен быть вызов функции изменения статуса на "идет регистрация"
          const flight_from_base = await getFlight(flight.id, db)
          if (flight_from_base.status === "Ожидание") {
            console.log(`Смена статуса на Идет регистрация ${flight.id}`)
            await changeStatusFlight("Идет регистрация", flight.id, db)
            sendMessage(
              "FlightsToRegistration",
              JSON.stringify({
                Flight: flight.id,
                Registration: 1,
              }),
              "FlightsToRegistrationKey"
            )
          }
        },
        pointATime - now > 180000 ? pointATime - now - 180000 : 1
      )

      setTimeout(async () => {
        // Здесь должен быть вызов функции изменения статуса на "регистрация закончена"
        const flight_from_base = await getFlight(flight.id, db)
        if (flight_from_base.status === "Идет регистрация") {
          console.log(`Смена статуса на Регистрация закончена ${flight.id}`)
          await changeStatusFlight("Регистрация закончена", flight.id, db)
          sendMessage(
            "FlightsToRegistration",
            JSON.stringify({
              Flight: flight.id,
              Registration: 0,
            }),
            "FlightsToRegistrationKey"
          )
        }
      }, pointATime - now - 60000)
      setTimeout(async () => {
        // Здесь должен быть вызов функции изменения статуса на "регистрация закончена"
        const flight_from_base = await getFlight(flight.id, db)
        if (flight_from_base.status === "Регистрация закончена") {
          console.log(`Смена статуса на Задерживается ${flight.id}`)
          await changeStatusFlight("Задерживается", flight.id, db)
        }
      }, pointATime - now)
    } else if (flight.status === "Регистрация закончена") {
      setTimeout(
        async () => {
          // Здесь должен быть вызов функции изменения статуса на "регистрация закончена"
          const flight_from_base = await getFlight(flight.id, db)
          if (flight_from_base.status === "Регистрация закончена") {
            console.log(`Смена статуса на Задерживается ${flight.id}`)
            await changeStatusFlight("Задерживается", flight.id, db)
          }
        },
        pointATime - now > 0 ? pointATime - now : 1
      )
    } else {
      // Продолбали рейс - отменяем
      const flight_from_base = await getFlight(flight.id, db)
      if (
        flight_from_base.status === "Ожидание" ||
        flight_from_base.status === "Идет регистрация"
      ) {
        console.log(`Смена статуса на Отменен ${flight.id}`)
        await changeStatusFlight("Отменен", flight.id, db)
        sendMessage(
          "FlightsToRegistration",
          JSON.stringify({
            Flight: flight.id,
            Registration: 2,
          }),
          "FlightsToRegistrationKey"
        )
        // sendMessage("DeleteBoard", `${flight.id}`, "FlightsToBoardKey")
      }
    }
  }
}

module.exports = { initDB, checkAndGenerateFlights, changeStatusFlight } // Экспортируем функции для возможного использования в других частях приложения
