const cities = [
  { city: "Санкт-Петербург", timeFlight: 4200 },
  { city: "Екатеринбург", timeFlight: 8400 },
  { city: "Новосибирск", timeFlight: 15600 },
  { city: "Казань", timeFlight: 5700 },
  { city: "Краснодар", timeFlight: 7200 },
  { city: "Сочи", timeFlight: 7800 },
  { city: "Владивосток", timeFlight: 28800 },
  { city: "Иркутск", timeFlight: 19800 },
  { city: "Хабаровск", timeFlight: 29400 },
  { city: "Уфа", timeFlight: 7800 },
  { city: "Самара", timeFlight: 6600 },
  { city: "Омск", timeFlight: 13200 },
  { city: "Челябинск", timeFlight: 9000 },
  { city: "Ростов-на-Дону", timeFlight: 7200 },
  { city: "Волгоград", timeFlight: 6600 },
  { city: "Пермь", timeFlight: 7800 },
  { city: "Воронеж", timeFlight: 4800 },
  { city: "Саратов", timeFlight: 6000 },
  { city: "Красноярск", timeFlight: 16200 },
  { city: "Тюмень", timeFlight: 10200 },
  { city: "Томск", timeFlight: 15000 },
  { city: "Якутск", timeFlight: 22800 },
  { city: "Улан-Удэ", timeFlight: 21000 },
  { city: "Владикавказ", timeFlight: 8400 },
  { city: "Махачкала", timeFlight: 9600 },
  { city: "Архангельск", timeFlight: 9000 },
  { city: "Нижневартовск", timeFlight: 10800 },
  { city: "Нижний Новгород", timeFlight: 4200 },
  { city: "Мурманск", timeFlight: 9600 },
  { city: "Петрозаводск", timeFlight: 5400 },
  { city: "Сыктывкар", timeFlight: 7800 },
  { city: "Ханты-Мансийск", timeFlight: 12000 },
  { city: "Анапа", timeFlight: 7200 },
  { city: "Белгород", timeFlight: 4800 },
  { city: "Великий Новгород", timeFlight: 4500 },
  { city: "Вологда", timeFlight: 4200 },
  { city: "Владимир", timeFlight: 4800 },
  { city: "Калининград", timeFlight: 6300 },
  { city: "Грозный", timeFlight: 9300 },
  { city: "Йошкар-Ола", timeFlight: 6600 },
  { city: "Кемерово", timeFlight: 15300 },
  { city: "Киров", timeFlight: 6900 },
  { city: "Курган", timeFlight: 9600 },
  { city: "Курск", timeFlight: 5400 },
  { city: "Липецк", timeFlight: 4800 },
  { city: "Магадан", timeFlight: 42600 },
  { city: "Магнитогорск", timeFlight: 8400 },
  { city: "Набережные Челны", timeFlight: 6000 },
  { city: "Нальчик", timeFlight: 7800 },
  { city: "Нарьян-Мар", timeFlight: 10800 },
  { city: "Новокузнецк", timeFlight: 14400 },
  { city: "Новороссийск", timeFlight: 7200 },
  { city: "Норильск", timeFlight: 13800 },
  { city: "Оренбург", timeFlight: 8400 },
  { city: "Орск", timeFlight: 9000 },
  { city: "Пенза", timeFlight: 5700 },
  { city: "Псков", timeFlight: 5100 },
  { city: "Салехард", timeFlight: 12600 },
  { city: "Саранск", timeFlight: 6000 },
  { city: "Севастополь", timeFlight: 9900 },
  { city: "Симферополь", timeFlight: 9000 },
  { city: "Ставрополь", timeFlight: 8100 },
  { city: "Стрежевой", timeFlight: 14700 },
  { city: "Тамбов", timeFlight: 5400 },
  { city: "Ульяновск", timeFlight: 6000 },
  { city: "Усинск", timeFlight: 10200 },
  { city: "Усть-Илимск", timeFlight: 16800 },
  { city: "Хасавюрт", timeFlight: 9300 },
  { city: "Чебоксары", timeFlight: 4800 },
  { city: "Череповец", timeFlight: 4200 },
  { city: "Чита", timeFlight: 21600 },
  { city: "Элиста", timeFlight: 8400 },
  { city: "Южно-Сахалинск", timeFlight: 27000 },
  { city: "Ярославль", timeFlight: 4200 },
]
const amqp = require("amqplib")
async function sendMessages(queueName, messages) {
  try {
    // Параметры подключения
    const amqpServer =
      "amqps://itojxdln:DEL8js4Cg76jY_2lAt19CjfY2saZT0yW@hawk.rmq.cloudamqp.com/itojxdln"
    const connection = await amqp.connect(amqpServer)
    const channel = await connection.createChannel()

    // Убедитесь, что очередь, в которую вы отправляете сообщения, существует.
    await channel.assertQueue(queueName, {
      durable: false,
    })

    // Create bindings
    await channel.bindQueue(
      "FlightsToPassengers",
      "PassengersExchange",
      "FlightsToPassengersKey"
    )

    // Отправляем каждое сообщение из массива
    messages.forEach((message) => {
      channel.publish(
        "PassengersExchange",
        "FlightsToPassengersKey",
        Buffer.from(message)
      )
      console.log(`Message sent to queue ${queueName}: ${message}`)
    })

    // Даем время на отправку сообщений перед закрытием соединения
    setTimeout(() => {
      connection.close()
    }, 500) // Время можно адаптировать под ваши нужды
  } catch (error) {
    console.error(`Error sending messages to queue ${queueName}: ${error}`)
  }
}
async function sendMessagesRegistr(queueName, messages) {
  try {
    // Параметры подключения
    const amqpServer =
      "amqps://itojxdln:DEL8js4Cg76jY_2lAt19CjfY2saZT0yW@hawk.rmq.cloudamqp.com/itojxdln"
    const connection = await amqp.connect(amqpServer)
    const channel = await connection.createChannel()

    // Убедитесь, что очередь, в которую вы отправляете сообщения, существует.
    await channel.assertQueue(queueName, {
      durable: false,
    })

    // Create bindings
    await channel.bindQueue(
      "FlightsToRegistration",
      "PassengersExchange",
      "FlightsToRegistrationKey"
    )

    // Отправляем каждое сообщение из массива
    messages.forEach((message) => {
      channel.publish(
        "PassengersExchange",
        "FlightsToRegistrationKey",
        Buffer.from(message)
      )
      console.log(`Message sent to queue ${queueName}: ${message}`)
    })

    // Даем время на отправку сообщений перед закрытием соединения
    setTimeout(() => {
      connection.close()
    }, 500) // Время можно адаптировать под ваши нужды
  } catch (error) {
    console.error(`Error sending messages to queue ${queueName}: ${error}`)
  }
}
async function sendMessagesTickets(queueName, message) {
  try {
    // Параметры подключения
    const amqpServer =
      "amqps://itojxdln:DEL8js4Cg76jY_2lAt19CjfY2saZT0yW@hawk.rmq.cloudamqp.com/itojxdln"
    const connection = await amqp.connect(amqpServer)
    const channel = await connection.createChannel()

    // Убедитесь, что очередь, в которую вы отправляете сообщения, существует.
    await channel.assertQueue(queueName, {
      durable: false,
    })

    // Create bindings
    await channel.bindQueue(
      "TicketsRequest",
      "PassengersExchange",
      "TicketsRequestKey"
    )

    // Отправляем каждое сообщение из массива
    channel.publish(
      "PassengersExchange",
      "TicketsRequestKey",
      Buffer.from(message)
    )
    console.log(`Message sent to queue ${queueName}: ${message}`)

    // Даем время на отправку сообщений перед закрытием соединения
    setTimeout(() => {
      connection.close()
    }, 500) // Время можно адаптировать под ваши нужды
  } catch (error) {
    console.error(`Error sending messages to queue ${queueName}: ${error}`)
  }
}

const { v4: uuidv4 } = require("uuid")

// Функция для генерации случайного числа в заданном диапазоне
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min
}

// Функция для выбора случайного города из списка
function getRandomCity(cities) {
  const index = getRandomInt(0, cities.length - 1)
  return cities[index]
}

// Функция для добавления секунд к времени
function addSecondsToTime(time, seconds) {
  return new Date(time.getTime() + seconds * 1000)
}

// Функция для преобразования времени в строку в формате ISO
function toISOString(time) {
  return time.toISOString()
}

// Основная функция генерации рейсов
function generateFlights(startTime, durationMinutes) {
  const flights = []
  let currentTime = new Date(startTime)
  console.log(currentTime.toISOString())
  const endTime = new Date(currentTime.getTime() + durationMinutes * 60000)

  while (true) {
    const lastFlightArray = flights[flights.length - 1]
    if (
      lastFlightArray &&
      new Date(lastFlightArray[0].point_b_time) > endTime
    ) {
      break
    }

    const commonGuid = uuidv4()
    const startupTime = lastFlightArray
      ? new Date(lastFlightArray[0].point_b_time)
      : currentTime
    const arriveTime = addSecondsToTime(startupTime, 60 + getRandomInt(1, 60))
    const departureTime = addSecondsToTime(
      arriveTime,
      240 + getRandomInt(1, 60)
    )
    const point_arrive = getRandomCity(cities)
    const point_departure = getRandomCity(cities)
    const airplane_capacity = getRandomInt(50, 150)

    const firstFlight = {
      id: uuidv4(),
      linkedId: commonGuid,
      type: "arrive",
      status: "В полете",
      point_a_time: toISOString(
        new Date(arriveTime.getTime() - point_arrive.timeFlight * 1000)
      ),
      point_a_city: point_arrive.city,
      point_b_time: toISOString(arriveTime),
      point_b_city: "Чапкинград",
      airplane_capacity,
    }

    const secondFlight = {
      id: uuidv4(),
      linkedId: commonGuid,
      type: "departure",
      status: "Ожидание",
      point_a_time: toISOString(departureTime),
      point_a_city: "Чапкинград",
      point_b_time: toISOString(
        new Date(departureTime.getTime() + point_departure.timeFlight * 1000)
      ),
      point_b_city: point_departure.city,
      airplane_capacity,
    }

    flights.push([firstFlight, secondFlight])
  }
  sendMessages(
    "PassengersExchange",
    flights
      .map((el) => el[1])
      .map((item) =>
        JSON.stringify({
          Flight: item.id,
          Date: item.point_a_time,
          TotalSeats: item.airplane_capacity,
        })
      )
  )
  sendMessagesRegistr(
    "PassengersExchange",
    flights
      .map((el) => el[1])
      .map((item) =>
        JSON.stringify({
          Flight: item.id,
          Registration: 3,
        })
      )
  )
  sendMessagesTickets("PassengersExchange", "update filghts")
  return flights
}

module.exports = generateFlights
