const express = require("express")
const cors = require('cors');
const {
  initDB,
  checkAndGenerateFlights,
  changeStatusFlight,
} = require("./database") // Импорт функции инициализации БД
const app = express()
const PORT = 9111
const amqp = require("amqplib")

async function connectRabbitMQ(db) {
  try {
    const amqpServer =
      "amqps://itojxdln:DEL8js4Cg76jY_2lAt19CjfY2saZT0yW@hawk.rmq.cloudamqp.com/itojxdln"
    const connection = await amqp.connect(amqpServer)
    const channel = await connection.createChannel()

    const exchangeName = "PassengersExchange"
    const queueName = "BoardToFlight"
    const routingKey = "BoardToFlightKey"

    await channel.assertExchange(exchangeName, "direct", { durable: false })
    await channel.assertQueue(queueName, { durable: false })
    await channel.bindQueue(queueName, exchangeName, routingKey)

    channel.consume(
      queueName,
      async (msg) => {
        if (msg !== null) {
          const messageContent = JSON.parse(msg.content.toString())
          const { id, status } = messageContent

          console.log(
            `Получено сообщение для рейса ID: ${id} с статусом: ${status}`
          )

          await changeStatusFlight(status, id, db)
          channel.ack(msg)
        }
      },
      { noAck: false }
    )
  } catch (error) {
    console.error("Ошибка при подключении или обработке сообщений:", error)
  }
}
app.use(cors());
app.use(express.json())
;(async () => {
  try {
    const db = await initDB() // Асинхронно инициализируем базу данных
    connectRabbitMQ(db).catch((error) =>
      console.error("Ошибка при подключении к RabbitMQ:", error)
    )
    // Маршрут для получения всех рейсов
    app.get("/flights", async (req, res) => {
      try {
        // await db.all("DROP TABLE flights")

        const rows = await db.all("SELECT * FROM flights")
        res.json({
          flights: rows,
        })
      } catch (err) {
        res.status(500).send({ error: err.message })
      }
    })
    app.get("/drop", async (req, res) => {
      try {
        await db.all("DROP TABLE flights")
        res.json({
          flights: "rows",
        })
      } catch (err) {
        res.status(500).send({ error: err.message })
      }
    })
    app.get("/api/v1/flights/arrive", async (req, res) => {
      try {
        const now = new Date().toISOString()
        const pastFlights = await db.all(
          "SELECT * FROM flights WHERE type = 'arrive' AND point_b_time < ? ORDER BY point_b_time DESC LIMIT 5",
          [now]
        )
        const futureFlights = await db.all(
          "SELECT * FROM flights WHERE type = 'arrive' AND point_b_time >= ? ORDER BY point_b_time ASC",
          [now]
        )
        const flights = [...pastFlights.reverse(), ...futureFlights] // Сначала прошедшие, затем будущие
        res.json({
          flights: flights.map((el) => ({
            status: el.status,
            point_b_time: el.point_b_time,
          })),
        })
      } catch (err) {
        res.status(500).send({ error: err.message })
      }
    })

    app.get("/api/v1/flights/departure", async (req, res) => {
      try {
        const now = new Date().toISOString()
        const pastFlights = await db.all(
          "SELECT * FROM flights WHERE type = 'departure' AND point_a_time < ? ORDER BY point_a_time DESC LIMIT 5",
          [now]
        )
        const futureFlights = await db.all(
          "SELECT * FROM flights WHERE type = 'departure' AND point_a_time >= ? ORDER BY point_a_time ASC",
          [now]
        )
        const flights = [...pastFlights.reverse(), ...futureFlights] // Сначала прошедшие, затем будущие
        res.json({ flights })
      } catch (err) {
        res.status(500).send({ error: err.message })
      }
    })

    app.get("/api/v1/flights/:id", async (req, res) => {
      try {
        const { id } = req.params
        const flight = await db.get("SELECT * FROM flights WHERE id = ?", [id])
        if (flight) {
          res.json(flight)
        } else {
          res.status(404).send({ error: "Рейс не найден" })
        }
      } catch (err) {
        res.status(500).send({ error: err.message })
      }
    })

    // Другие маршруты...

    app.listen(PORT, "0.0.0.0", () => {
      console.log(
        `Сервер запущен и доступен по адресу http://46.174.48.185:${PORT}`
      )
    })
    setInterval(() => {
      checkAndGenerateFlights(db).catch((error) => console.error(error))
    }, 60000)
  } catch (error) {
    console.error("Не удалось инициализировать базу данных", error)
  }
})()

// FlightsToPassengers
